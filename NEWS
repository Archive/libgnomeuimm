2.28.0:

Increased version for GNOME 2.28.

2.26.0:

* Cleaned up glib includes to use only toplevel headers.
  (Przemysław Grzegorczyk) Bug #564222
  
2.24.0:

Increased version for GNOME 2.24.

2.22.0:

Increased version for GNOME 2.22.

2.20.2:

* Possible extra fix for gcc 4.3 pre-releases.
  (hodoscek)

2.20.1:

* Build fixes with gcc 4.3 pre-releases.
  (Stanislav Brabec)
* Require newer versions of dependencies.
  (Murray Cumming)

2.20.0:

* Updated version number to match GNOME version.

2.18.0:

* Updated version number to match GNOME version.

2.16.0:

* Depend on latest libgnomemm, and use the 
new non-depecated Main constructor in the example.
(Michael Terry)

2.14.0:

* Increased version for GNOME 2.14 release version.

2.12.0:

* Increased version for GNOME 2.12 release version.

2.10.0:

* Increased version for GNOME 2.10 release version.

2.8.0:

* Increased version for GNOME 2.8 release version.

2.7.2: (first 2.7 version)

* IconList: Added select_all().
* DruidPageStandard: Added contents-background, 
  contents-background-gdk, and contents-background-set properties.

2.6.0:

Increased version.

2.5.4:

* Now depends on gnome-vfsmm.
* Added icon-lookup() and icon_lookup_sync() functions.
* Added ThumnailFactory.

2.5.3:

* Stock items: Fix linker error when using these.
  (Bryan Forbes)

2.5.2:

* PixmapEntry: Correct the base class.
* Entry: Really deprecate it.
  (Bryan Forbes)

2.5.1:

* Updated for libsigc++2 API (Murray Cumming).
* examples/iconlist: Updated for latest gtkmm
  DND API changes. (Bryan Forbes)
* Spec file dependency fix. (Eric Bourque)

2.5.0:
 
This is the new unstable branch of the parallel-installable 
libgnomeuimm 2.6, for use with gtkmm 2.4.

2.0.0:

* Removed libbonobouimm dependency - we now use the C type in the few 
  places that use BonoboDock in the API. This should make zero difference
  for most people, but will make packaging easier.
  (Murray Cumming)
* Gnome::UI::Info inherits publically from the C type, to fix compilation
  with gcc 3.3 on debian, though it might be a compiler bug.
  (Murray Cumming, Bradley Bell)

1.3.17:

* DateEdit:
  - Fixed the constructor so that these widgets are properly initialized.
  - Wrapped the DateEditFlags enum as C++.
  (Bryan Forbes, Murray Cumming)

1.3.16:

* Fixed XPM UI items to work with included .xpm files
  (Gergo Erdi)
* Entry::gtk_entry() is now get_gtk_entry() and returns a 
  Gtk::Entry* instead of a Gtk::Widget*. (Gergo Erdi)

1.3.15:

* libbonobouimm requirement increased.

1.3.14:

* UI::Items::fill() is now correctly defined and returns the filled-in array of
  UI::Items::Info elements so it can be used to access the underlying
  Gtk::Widgets directly. (Gergo Erdi)
* FileEntry inherits from Gtk::VBox instead of Gtk::HBox, as it does in the C API.
  (Murray Cumming)

1.3.13:

* Gnome::UI::Items API and code cleaned up. The invisible/duplicated toolbar icons bug has been solved.
  (Murray Cumming)
* Fixed memory errors detected by Valgrind
  (Matthew Tuck)
* Gnome::UI::Items::fill() properly implemented.
  (Tassos Bassoukos)
* examples fixed for gcc 3.2 (Murray Cumming)
