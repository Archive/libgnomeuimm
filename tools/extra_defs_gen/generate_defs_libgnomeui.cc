/* $Id$ */

/* generate_defs_libgnomeui.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "glibmm_generate_extra_defs/generate_extra_defs.h"
#include <libgnomeui/libgnomeui.h>

#include <gtk/gtk.h>

int main (int argc, char *argv[])
{
  gtk_init(&argc, &argv);

  std::cout << get_defs( GNOME_TYPE_ABOUT )
            << get_defs( GNOME_TYPE_APP )
            << get_defs( GNOME_TYPE_APPBAR )
            << get_defs( GNOME_TYPE_CLIENT )
            << get_defs( GNOME_TYPE_COLOR_PICKER )
            << get_defs( GNOME_TYPE_DATE_EDIT )
            << get_defs( GNOME_TYPE_DRUID_PAGE_EDGE )
            << get_defs( GNOME_TYPE_DRUID_PAGE_STANDARD )
            << get_defs( GNOME_TYPE_DRUID_PAGE )
            << get_defs( GNOME_TYPE_DRUID )
            << get_defs( GNOME_TYPE_ENTRY )
            << get_defs( GNOME_TYPE_FILE_ENTRY )
            << get_defs( GNOME_TYPE_FONT_PICKER )
            << get_defs( GNOME_TYPE_HREF )
            << get_defs( GNOME_TYPE_ICON_ENTRY )
            << get_defs( GNOME_TYPE_ICON_LIST )
            << get_defs( GNOME_TYPE_ICON_SELECTION )
            << get_defs( GNOME_TYPE_ICON_TEXT_ITEM )
            << get_defs( GNOME_TYPE_PASSWORD_DIALOG )
            << get_defs( GNOME_TYPE_PIXMAP_ENTRY )
            << get_defs( GNOME_TYPE_PIXMAP )
            << get_defs( GNOME_TYPE_SCORES )
            << get_defs( GNOME_TYPE_THUMBNAIL_FACTORY );
//            << get_defs( GNOME_TYPE_UI_INFO_TYPE )
//            << get_defs( GNOME_TYPE_UI_INFO_CONFIGURABLE_TYPES )
//            << get_defs( GNOME_TYPE_UI_PIXMAP_TYPE )
//            << get_defs( GNOME_TYPE_INTERACT_STYLE )
//            << get_defs( GNOME_TYPE_DIALOG_TYPE )
//            << get_defs( GNOME_TYPE_SAVE_STYLE )
//            << get_defs( GNOME_TYPE_RESTART_STYLE )
//            << get_defs( GNOME_TYPE_CLIENT_STATE )
//            << get_defs( GNOME_TYPE_CLIENT_FLAGS )
//            << get_defs( GNOME_TYPE_DATE_EDIT_FLAGS )
//            << get_defs( GNOME_TYPE_EDGE_POSITION )
//            << get_defs( GNOME_TYPE_FONT_PICKER_MODE )
//            << get_defs( GNOME_TYPE_ICON_LIST_MODE )
//           << get_defs( GNOME_TYPE_MDI_MODE )
//            << get_defs( GNOME_TYPE_PREFERENCES_TYPE );

  return 0;
}
