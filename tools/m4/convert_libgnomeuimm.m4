_CONVERSION(`char**',`gchar*[]',`$3')
_CONVERSION(`guint8&',`guint8*',`&($3)')
_CONVERSION(`gushort&',`gushort*',`&($3)')

_CONV_ENUM(Gnome,ThumbnailSize)

_CONVERSION(Gtk::Editable&,GtkEditable*,`$3.gobj()')
_CONVERSION(`GtkProgressBar*',`Gtk::ProgressBar*',`Glib::wrap($3)')
_CONVERSION(`Gtk::Dialog&',`GtkDialog*',`$3.gobj()')
_CONVERSION(`GnomeClient*',`Client*',`Glib::wrap($3)')
_CONVERSION(`GnomeIconTextItem*',`IconTextItem*',`Glib::wrap($3)')
_CONVERSION(`GtkEditable*',`Glib::RefPtr<Gtk::Editable>',`Glib::wrap($3)')

#_CONVERSION(GtkObject*,`Gnome::MDI*',__FP2PD,__RP2PD)
#_CONVERSION(Gtk::Widget&,GtkWidget*,`$3.gobj()')
_CONVERSION(`GtkWidget*', `Gtk::Widget&', `*(Glib::wrap($3))')
_CONVERSION(Gnome::App&,`$3.gobj()',GnomeApp*,`*Glib::wrap($3)')
# _CONVERSION(Gtk::VBox*,GtkWidget*,__FP2PD,__RP2PD)
_CONVERSION(`GtkWidget*', `Gtk::VBox*', `Glib::wrap((GtkVBox*)$3)')
#_CONVERSION(Gtk::Window&,GtkWindow*,`$3.gobj()')

###
_CONVERSION(`Gtk::Container&',`GtkWidget*',`$3.Gtk::Widget::gobj()')
_CONVERSION(`Gtk::MenuBar&',`GtkMenuBar*',`$3.gobj()')
_CONVERSION(Gtk::MenuBar&,GtkMenuBar*,`$3.gobj()')
_CONVERSION(Gtk::Toolbar&,GtkToolBar*,`$3.gobj()')
_CONVERSION(Gnome::Pixmap&,GnomePixmap*,`$3.gobj()')

_CONVERSION(Gnome::Bonobo::Dock&,BonoboDock*,`$3.gobj()')
_CONVERSION(Gnome::Bonobo::DockItem&,BonoboDockItem*,`$3.gobj()')
_CONVERSION(`BonoboDockItem*',`Gnome::Bonobo::DockItem*',`Glib::wrap($3)')
_CONVERSION(`BonoboDock*',`Gnome::Bonobo::Dock*',`Glib::wrap($3)')

_CONVERSION(`GtkWidget*',`Gtk::ScrolledWindow*',`Glib::wrap((GtkScrolledWindow*)$3)')
_CONVERSION(`GtkWidget*',`const Gtk::ScrolledWindow*',`Glib::wrap((GtkScrolledWindow*)$3)')
#_CONVERSION(GtkWidget*,Gnome::Dock*,__FP2PD,__RP2PD)
# _CONVERSION(GtkWidget*,Gnome::Dialog*,__FP2PD,__RP2PD)
# _CONVERSION(GnomeCanvasItem*,Gnome::Canvas::Group*,,`Glib::wrap(GNOME_CANVAS_GROUP($3))')
# _CONVERSION(GnomeCanvas*,Gnome::Canvas*,,`Glib::wrap($3)')

#
# Other Gnome Conversions
#
_CONVERSION(GnomeUIInfo*,const Gnome::UIInfo&,`const_cast<Gnome::UIInfo*>(&$3)')
_CONVERSION(GnomeUIInfo*,Gnome::UIInfo*,$3)
_CONVERSION(GnomeUIInfo*,Gnome::UIInfoTree&,$3.gobj())
_CONVERSION(GnomeMDIChild*,`Gnome::MDIChild&',`$3.gobj()')
_CONVERSION(GnomeMDIChild*,`Gnome::MDIChild*',,`Glib::wrap($3)')
# _CONVERSION(GnomeCanvasGroup*,`Gnome::Canvas::Group*',,`Glib::wrap($3)')
# _CONVERSION(GnomeCanvasItem*,`Gnome::Canvas::Item*',,`Glib::wrap($3)')
_CONVERSION(GnomeCanvasPixbuf*,`Gnome::Canvas::Pixbuf*',`Glib::wrap($3)')
_CONVERSION(GnomeApp*,`Gnome::App*',,`Glib::wrap($3)')
_CONVERSION(GnomeApp*,`const Gnome::App&',`const_cast<GnomeApp*>($3.gobj())')
_CONVERSION(`const DruidPage&',`GnomeDruidPage*',`const_cast<GnomeDruidPage*>($3.gobj())')
_CONVERSION(`GnomeDruidPage*',`DruidPage*',`Glib::wrap($3)')

_CONVERSION( GnomeMDIMode,Mode,`$1($3)',`$2($3)')

_CONVERSION(`GdkColor', `Gdk::Color', `Glib::wrap(const_cast<GdkColor*>(&($3)), true)')

_CONVERSION(`EdgePosition', `GnomeEdgePosition', `(GnomeEdgePosition)($3)')
_CONVERSION(`PreferencesType', `GnomePreferencesType', `(GnomePreferencesType)($3)')
_CONVERSION(`SaveStyle', `GnomeSaveStyle', `(GnomeSaveStyle)($3)')
_CONVERSION(`GnomeSaveStyle', `SaveStyle', `(SaveStyle)($3)')
_CONVERSION(`InteractStyle', `GnomeInteractStyle', `(GnomeInteractStyle)($3)')
_CONVERSION(`GnomeInteractStyle', `InteractStyle', `(InteractStyle)($3)')

_CONVERSION(`const Glib::RefPtr<Gdk::Pixbuf>&', `GdkPixbuf*', `($3)->gobj()')

_CONVERSION(GtkEditable*,`Glib::RefPtr<const Gtk::Editable>',`Glib::wrap($3)')

_CONVERSION(`GnomeDateEditFlags', `DateEditFlags', `static_cast<$2>($3)')
_CONVERSION(`DateEditFlags', `GnomeDateEditFlags', `static_cast<$2>($3)')
_CONVERSION(`gint',`DateEditFlags',`static_cast<$2>($3)')
