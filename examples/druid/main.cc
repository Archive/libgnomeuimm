// -*- C++ -*-

/* druiddemo.cc
 * 
 * Copyright (C) 2000 Gtk-- Development Team  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "druid-window.h"

int main (int argc, char *argv[])
{
  Gnome::Main gnomeMain("GnomeDruid", "0.1", Gnome::UI::module_info_get(), argc, argv);

  Window_DruidDemo windowDemo;
  gnomeMain.run(windowDemo);

  return 0;
}
