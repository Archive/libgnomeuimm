// -*- C++ -*-

/* druid-window.cc
 * 
 * Copyright (C) 2000 Gtk-- Development Team  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "dialog_druid.h"

#include <iostream>


Dialog_Druid::Dialog_Druid()
: m_DruidPage1(Gnome::UI::EDGE_START),
  m_Page2_Label("some info"),
  m_DruidPage3(Gnome::UI::EDGE_FINISH)
{
  m_bFinished = false;
  
  set_title("Gnome-- DruidDemo");
  set_default_size(400, 400);

  //Build Pages:

  //Page 1:
  m_DruidPage1.set_title("Example Druid - Page 1");
  m_DruidPage1.set_text("This is a Gnome::DruidPageStart. \n\
    It seems that it should just contain text via the set_text() method, \n\
    not extra widgets. You can also change the title, colors, and logo.");

  //Page 2:
  m_DruidPage2.set_title("Example Druid - Page 2");
  m_Page2_HBox.pack_start(m_Page2_Label);
  m_Page2_HBox.pack_start(m_Page2_Entry);
  
  //Gtk::VBox* pBox = Gtk::wrap(GTK_VBOX(m_DruidPage2.gtkobj()->vbox));
  Gtk::VBox* pBox = m_DruidPage2.get_vbox();
  if(pBox)
    pBox->pack_start(m_Page2_HBox);

  //Page 3:
  m_DruidPage3.set_title("Example Druid - Page 3");
  m_DruidPage3.set_text("This is a Gnome::DruidPageFinish. \n\
    This class is very similar to Gnome::DruidPageStart.");
  
  //Add Pages to Druid:
  m_Druid.append_page(m_DruidPage1);
  m_Druid.append_page(m_DruidPage2);
  m_Druid.append_page(m_DruidPage3);
  
  m_Druid.show_all();
  get_vbox()->pack_start(m_Druid);


  //Connect signals:
  m_Druid.signal_cancel().connect( sigc::mem_fun(*this, &Dialog_Druid::on_Druid_cancel) ); //The whole Druid.
  m_DruidPage3.signal_finish().connect( sigc::mem_fun(*this, &Dialog_Druid::on_Druid_finish) ); //The druid page with the [Finish] button.
    
}



Dialog_Druid::~Dialog_Druid()
{
}

void Dialog_Druid::on_Druid_cancel()
{
  //When the user clicks [Cancel], just close the dialog.
  //You could do some validation here.
  m_bFinished = false;
  hide();
  response(GTK_RESPONSE_CANCEL); //stops run().
}

void Dialog_Druid::on_Druid_finish(Gtk::Widget& druid)
{
  //When the user clicks [Finish], just close the dialog.
  //You could do some validation here.
  m_bFinished = true;
  hide();
  response(GTK_RESPONSE_OK); //stops run().
}

Glib::ustring Dialog_Druid::get_foo_value()
{
  return m_Page2_Entry.get_text();
}

bool Dialog_Druid::get_finished()
{
  return m_bFinished;
}

void Dialog_Druid::reset_druid()
{
  m_Druid.set_page(m_DruidPage1);
}


