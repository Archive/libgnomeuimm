// -*- C++ -*-

/* druid-window.h
 * 
 * Copyright (C) 2000 Gtk-- Development Team  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_WINDOW_DRUIDDEMO
#define HEADER_WINDOW_DRUIDDEMO

#include "dialog_druid.h"

#include <libgnomeuimm.h>
#include <gtkmm.h>
#include <gtkmm/window.h>
#include <iostream>
#include <string>


using sigc::bind;
using sigc::mem_fun;

class Window_DruidDemo : public Gtk::Window
{
public:
  Window_DruidDemo();
  virtual ~Window_DruidDemo();
  
protected:

  //Signal handlers:
  virtual void On_Button_RunDruid();
  
protected:
  Gtk::VBox m_VBox;
  Gtk::HBox m_HBox;
  Gtk::Button m_Button_RunDruid;
  Dialog_Druid m_Dialog;
};

#endif //HEADER_WINDOW_DRUIDDEMO
