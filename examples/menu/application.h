#include <libgnomeuimm/app.h>
#include <libgnomeuimm/appbar.h>

#include <gtkmm.h>

#include <vector>
#include <list>

class AppExample : public Gnome::UI::App
{
public:
  AppExample();
  virtual ~AppExample();

protected:

  virtual void init();

  virtual void install_menus();
  virtual void install_toolbars();
  virtual void close();

  //Signal handlers:
  virtual void on_menu_generic();
  virtual void on_menu_file_exit();

  virtual void on_button_insert();
  virtual void on_button_disable();
  virtual void on_button_enable();


  typedef std::vector<Gnome::UI::Items::SubTree> type_vecGnome_UI_SubTree;
  typedef std::vector<Gnome::UI::Items::Info> type_vecGnome_UI_Info;
  
  //Member widgets:
  Gnome::UI::AppBar m_Status;
  Gtk::VBox m_VBox;
  Gtk::HButtonBox m_HBox;
  Gtk::Button m_Button_Insert, m_Button_Disable, m_Button_Enable;

  Gtk::Widget* m_pMenuItem;
  Gtk::Widget* m_pToolbarItem;
};

