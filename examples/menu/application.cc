/* application.cc
 *
 * Copyright (C) 1999 Havoc Pennington, The Gtk-- Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "application.h"

#include <libgnomeuimm/about.h>
#include <gtkmm/menu.h>
#include <gtkmm/menubar.h>
#include <gtkmm/stock.h>
#include <libgnome/gnome-i18n.h>
#include <iostream>
#include <algorithm>

AppExample::AppExample():
  Gnome::UI::App("ExampleApp", "Example App"),
  m_Button_Insert("Insert submenu items"),
  m_Button_Disable("Disable Save menu item"),
  m_Button_Enable("Enable Save menu item")
{
  m_pMenuItem = 0;
  
  init();
  show_all();
}

AppExample::~AppExample()
{
}


void
AppExample::init()
{ 
  //set_policy(false, true, false);
  set_default_size(250, 350);
  set_wmclass("exampleapp", "ExampleApp");

  //Connect signals:
  m_Button_Insert.signal_clicked().connect(sigc::mem_fun(*this, &AppExample::on_button_insert));
  m_Button_Disable.signal_clicked().connect(sigc::mem_fun(*this, &AppExample::on_button_disable));
  m_Button_Enable.signal_clicked().connect(sigc::mem_fun(*this, &AppExample::on_button_enable));
  
  m_HBox.pack_start(m_Button_Insert);
  m_HBox.pack_start(m_Button_Disable);
  m_HBox.pack_start(m_Button_Enable);

  m_VBox.pack_end(m_HBox, Gtk::PACK_SHRINK);
  set_contents(m_VBox);
  m_HBox.show_all();
  
  
  set_statusbar(m_Status);
  
  install_menus();
  install_toolbars();
}

void
AppExample::install_menus()
{
  type_vecGnome_UI_SubTree menu_UI_Infos; //whole menu.
  
  // File menu
  type_vecGnome_UI_Info menu_file; //file menu.
  
  //Build menu:
  menu_file.push_back(Gnome::UI::Items::SubTree("Sub Menu",
    type_vecGnome_UI_Info(),
    "Sub menu items will be inserted here."));
  menu_file.push_back(Gnome::UI::MenuItems::Save(sigc::mem_fun(*this, &AppExample::on_menu_generic)));
  guint posFileSave = menu_file.size() - 1; //remember for later.
  
  menu_file.push_back(Gnome::UI::Items::Separator());
  menu_file.push_back(Gnome::UI::MenuItems::Exit(sigc::mem_fun(*this, &AppExample::on_menu_file_exit)));

  //Add menu:
  menu_UI_Infos.push_back(Gnome::UI::Menus::File(menu_file));

  
  //Edit menu
  type_vecGnome_UI_Info menu_edit;
  
  //Build menu:
  menu_edit.push_back(Gnome::UI::MenuItems::Cut(sigc::mem_fun(*this, &AppExample::on_menu_generic)));
  menu_edit.push_back(Gnome::UI::MenuItems::Copy(sigc::mem_fun(*this, &AppExample::on_menu_generic)));
  menu_edit.push_back(Gnome::UI::MenuItems::Paste(sigc::mem_fun(*this, &AppExample::on_menu_generic)));
  menu_edit.push_back(Gnome::UI::MenuItems::Clear(sigc::mem_fun(*this, &AppExample::on_menu_generic)));
  
  //Add menu:
  menu_UI_Infos.push_back(Gnome::UI::Menus::Edit(menu_edit));
  
  
  //Create menus and examine the result:
  Gnome::UI::Items::Array<Gnome::UI::Items::SubTree>& arrayInfo = create_menus(menu_UI_Infos);
  
  //Get created File info:
  Gnome::UI::Items::SubTree& subtreeFile = arrayInfo[0];
  Gnome::UI::Items::Array<Gnome::UI::Items::Info>& arrayInfoFile =
  subtreeFile.get_uitree(); //vector of menu item Gnome::UI::Items::Info.
  
  //Get created File/Save widget from info:
  m_pMenuItem = arrayInfoFile[posFileSave].get_widget();
  

  install_menu_hints();
}

void
AppExample::install_toolbars()
{
  type_vecGnome_UI_Info toolbar; 
  
  toolbar.push_back(Gnome::UI::Items::Item(Gnome::UI::Items::Icon(Gtk::Stock::SAVE),
    N_("Save "),
    sigc::mem_fun(*this, &AppExample::on_menu_generic),
    N_("Save this document")));
  guint posSave = toolbar.size() - 1; //Remember for later.
  
  toolbar.push_back(Gnome::UI::Items::Item(Gnome::UI::Items::Icon(Gtk::Stock::GO_BACK),
    N_("Prev"),
    sigc::mem_fun(*this, &AppExample::on_menu_generic),
    N_("Previous hello")));
  toolbar.push_back(Gnome::UI::Items::Item( Gnome::UI::Items::Icon(Gtk::Stock::GO_FORWARD),
    N_("Next"),
    sigc::mem_fun(*this, &AppExample::on_menu_generic),
    N_("Next hello")));
  
  Gnome::UI::Items::Array<Gnome::UI::Items::Info>& arrayInfo = create_toolbar(toolbar);
  m_pToolbarItem = arrayInfo[posSave].get_widget();
}


void     
AppExample::close()
{
  hide();
}

void
AppExample::on_menu_file_exit()
{
  close();
}

void
AppExample::on_menu_generic()
{
  std::cout << "Signal handler called." << std::endl;
}


void
AppExample::on_button_insert()
{
  type_vecGnome_UI_Info vecUI_Info;
  vecUI_Info.push_back(
  Gnome::UI::Items::Item("Inserted Item",
    sigc::mem_fun(*this, &AppExample::on_menu_generic),
    "Dynamically inserted item."));
  
  insert_menus("File/Sub Menu/", vecUI_Info); 
}

void
AppExample::on_button_disable()
{
  if(m_pMenuItem)
    m_pMenuItem->set_sensitive(false);
  
  if(m_pToolbarItem)
    m_pToolbarItem->set_sensitive(false);
}

void
AppExample::on_button_enable()
{
  if(m_pMenuItem)
    m_pMenuItem->set_sensitive(true);

  if(m_pToolbarItem)
    m_pToolbarItem->set_sensitive(true);
}
