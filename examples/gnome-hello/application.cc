/* app.cc
 *
 * Copyright (C) 1999 Havoc Pennington, The Gtk-- Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*** gnomehello-app */

#include "application.h"
//#include <gnome.h>
#include <libgnome/gnome-i18n.h>
#include <gtkmm/menu.h>
#include <gtkmm/menubar.h>
#include <libgnomeuimm.h>
#include <gtkmm/messagedialog.h>
//#include <gnome.h> //For stock icons.
#include <cstdio>
#include <algorithm> //For std::reverse().

// Keep a list of all open application windows


void AppSet::on_app_hide(Hello_App* pApp)
{
  instances_.remove(pApp);
  delete pApp;

  if(instances_.empty())
    Gtk::Main::quit();
}

void AppSet::add_app(Hello_App* pApp)
{
  instances_.push_back(pApp);
}

void AppSet::close_all()
{
  std::list<Hello_App*>::iterator i = instances_.begin();
  while (i != instances_.end())
  {
    (*i)->hide();
    i = instances_.begin();
  }
}


Hello_App::Hello_App(const Glib::ustring& message)
  : Gnome::UI::App("GnomeHello", "Gnome Hello"),
          label_(_("Hello, World!")),
           about_(0)
{
  init();
  show_all();
}

Hello_App::Hello_App(const Glib::ustring& message,  const std::vector<Glib::ustring> &greet)
: Gnome::UI::App("GnomeHello", "Gnome Hello"),
         about_(0)
{

  if(message.length())
    label_.set_text(message);
  else
    label_.set_text("Hello, World!");

  // gnomehello-widgets
  //
  init();
  
  // Greetings
  //
  if (greet.size())
  {
    std::string greetings(_("Special Greetings to:\n"));
    std::vector<Glib::ustring>::const_iterator i;
    for(i = greet.begin(); i != greet.end(); i++)
    {
      greetings += *i;
      greetings += '\n';
    }

    Gtk::MessageDialog dialogOK(*this, greetings);
    dialogOK.run();
  }

  show_all();
}

Hello_App::~Hello_App()
{
}


void
Hello_App::init()
{
  set_resizable();
  set_default_size(250, 350);
  set_wmclass("hello", "GnomeHello");

  frame_.set_shadow_type(Gtk::SHADOW_IN);

  button_.set_border_width(10);

  button_.add(label_);
  frame_.add(button_);

  set_contents(frame_);
  //add(frame_);

  set_statusbar(status_);

  install_menus_and_toolbar();
  
  // gnomehello-signals
  signal_hide().connect( sigc::bind<Hello_App*>(sigc::mem_fun(appset, &AppSet::on_app_hide), this) );
  button_.signal_clicked().connect( sigc::mem_fun(*this, &Hello_App::on_button_clicked) );

  // Register an instance of myself
  appset.add_app(this);
}

void
Hello_App::install_menus_and_toolbar()
{
  std::vector<Gnome::UI::Items::SubTree> menus;
  std::vector<Gnome::UI::Items::Info> file_menu, edit_menu, help_menu;
  {
    // This limits use of stock MenuItems to a small scope.
    using namespace Gnome::UI::MenuItems;

    // File menu
    file_menu.push_back( New(_("New hello"), _("Creates new hello"), sigc::mem_fun(*this, &Hello_App::on_menu_file_new)) );
    file_menu.push_back( Open() );
    file_menu.push_back( Save() );
    file_menu.push_back( SaveAs() );
    file_menu.push_back( Gnome::UI::Items::Separator() );
    file_menu.push_back( Close(sigc::mem_fun(*this, &Hello_App::on_menu_file_close)) );
    file_menu.push_back( Exit(sigc::mem_fun(*this, &Hello_App::on_menu_file_exit)) );

    // Edit Menu (stubs)
    edit_menu.push_back(Cut());
    edit_menu.push_back(Copy());
    edit_menu.push_back(Paste());
    edit_menu.push_back(SelectAll());
    edit_menu.push_back(Clear());
    edit_menu.push_back(Undo());
    edit_menu.push_back(Redo());
    edit_menu.push_back(Find());
    edit_menu.push_back(FindAgain());
    edit_menu.push_back(Replace());
    edit_menu.push_back(Properties());

    // Help Menu
    // this pulls in all the helps items
    help_menu.push_back(Gnome::UI::Items::Help("gnomemm_hello"));
    help_menu.push_back(About(sigc::mem_fun(*this, &Hello_App::on_menu_help_about)));
  }

  {
    // This limits use of stock MenuItems to a small scope.
    using namespace Gnome::UI::Menus;

    menus.push_back(File(file_menu));
    menus.push_back(Edit(edit_menu));
    menus.push_back(Help(help_menu));
  }

  create_menus(menus);
  install_menu_hints();


  // Toolbar
  //

  std::vector<Gnome::UI::Items::Info> toolbar;
  {
    using namespace Gnome::UI::Items;
    toolbar.push_back(Item(Icon(Gtk::Stock::NEW),
                           _("New Hello"),
                           sigc::mem_fun(*this, &Hello_App::on_menu_file_new),
                           _("Create a new hello")));
    toolbar.push_back(Separator());
    toolbar.push_back(Item(Icon(Gtk::Stock::GO_BACK),
                           _("Prev"),
                           sigc::mem_fun(*this, &Hello_App::nothing_cb),
                           _("Previous hello")));
    toolbar.push_back(Item(Icon(Gtk::Stock::GO_FORWARD),
                           _("Next"),
                           sigc::mem_fun(*this, &Hello_App::nothing_cb),
                           _("Next hello")));
                        
    toolbar.push_back(Item(Icon(Gnome::UI::Stock::ABOUT),
                           _("About"),
                           sigc::mem_fun(*this, &Hello_App::nothing_cb),
                           _("GnomeUI Stock About item")));
    create_toolbar(toolbar);
  }

}

void       
Hello_App::on_menu_file_close()
{
  hide();
}

void 
Hello_App::on_button_clicked()
{
  // reverse the string.
  std::string s = label_.get_text();
  std::reverse(s.begin(), s.end());
  label_.set_text(s);
}

void
Hello_App::nothing_cb()
{
  Gtk::MessageDialog dialog(*this,  _("This does nothing; it is only a demonstration."));
  dialog.run();
}


void
Hello_App::on_menu_file_exit()
{
  // we don't want to quit directly as we should save our work
  // therefore we need to send close to each window.
  appset.close_all();
}

void
Hello_App::on_menu_file_new()
{
  new Hello_App(_("Hello, World!"));
}

void
Hello_App::on_about_hide()
{
  if(about_)
  {
    delete about_; //Destroys and closes it.
    about_ = 0;
  }
}

void
Hello_App::on_menu_help_about()
{
  if(about_) // "About" box hasn't been closed, so just raise it
  {
    Glib::RefPtr<Gdk::Window> refAboutWin = about_->get_window();
    refAboutWin->show();
    refAboutWin->raise();
  }
  else
  {
    std::vector<Glib::ustring> authors;
    authors.push_back("Murray Cumming <murrayc@usa.net>");
    authors.push_back("Karl Nelson <kenelson@ece.ucdavis.edu>");
    authors.push_back("Guillaume Laurent <glaurent@worldnet.fr>");
    authors.push_back("Havoc Pennington <hp@pobox.com>");

    std::vector<Glib::ustring> documenters;

    Glib::RefPtr<Gdk::Pixbuf> refLogo = Gdk::Pixbuf::create_from_file("gnome-hello-logo.png");

    about_ = new Gnome::UI::About(_("GnomeHello"), "0.1",
                             "(C) 2000-2001 libgnomeuimm Development Team",
                             authors, documenters,
                             _("A sample libgnomeuimm application."), "",
                             refLogo);

    about_->set_transient_for(*this);
    about_->signal_hide().connect(sigc::mem_fun(*this, &Hello_App::on_about_hide));
    about_->show();
  }
}

