/* hello.cc
 *
 * Copyright (C) 1999 Havoc Pennington, The Gtk-- Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


//#include <gnome.h>
#include <libgnomeuimm.h>

#include "application.h"
#include <libgnome/gnome-i18n.h>


/*** gnomehello-optiongroup */
class Hello_OptionGroup : public Glib::OptionGroup
{ 
public:
  Hello_OptionGroup();

  bool greet_;
  vecustrings people_;
  Glib::ustring message_;
} options;

Hello_OptionGroup::Hello_OptionGroup()
: Glib::OptionGroup("hello_group", "arguments to the hello program"),
  greet_(FALSE), people_(), message_("")
{
  Glib::OptionEntry entry1;
  entry1.set_long_name("greet");
  entry1.set_short_name('g');
  entry1.set_description(N_("Say hello to specific people listed on the command line"));
  add_entry(entry1, greet_);

  Glib::OptionEntry entry2;
  entry2.set_long_name("message");
  entry2.set_short_name('m');
  entry2.set_description(N_("Specify a message other than \"Hello, World!\""));
  entry2.set_arg_description(N_("MESSAGE"));
  add_entry(entry2, message_);

  Glib::OptionEntry entry3;
  entry3.set_long_name(G_OPTION_REMAINING);
  add_entry(entry3, people_);
}
/* gnomehello-optiongroup ***/

class Gnome_Hello : public sigc::trackable
{
public:

  Gnome_Hello();

protected:
  // sig handlers

  void session_die(gpointer client_data);

  bool save_session(gint phase,
		    GnomeSaveStyle save_style,
		    bool is_shutdown, GnomeInteractStyle interact_style,
		    bool is_fast);

protected:
  Gnome::UI::Client* client_;
  
};

Gnome_Hello::Gnome_Hello()
  : client_(Gnome::UI::Client::master_client())
{
//   client_->save_yourself.connect(sigc::mem_fun(this, &Gnome_Hello::save_session));
//   client_->die.connect(sigc::mem_fun(this, &Gnome_Hello::session_die));
}

int 
main(int argc, char* argv[])
{
  // gnomehello-parsing */

//   bindtextdomain("GnomeHello", "");  // GNOMELOCALEDIR
//   textdomain("GnomeHello");

  Glib::OptionContext context;
  context.set_main_group(options);

  Gnome::Main gnomeMain("gnome-hello", "0.1",
                        Gnome::UI::module_info_get(), argc, argv,
                        context);

  if (options.greet_ && options.people_.empty())
  {
    g_printerr(_("You must specify someone to greet.\n"));
    exit(1);
  }
  else if (!options.greet_ && !options.people_.empty())
  {
    g_printerr(_("Command line arguments are only allowed with --greet.\n"));
    exit(1);
  }


  // gnomehello-client
  //
  Gnome_Hello client;

  Hello_App* myApp = new Hello_App(options.message_, options.people_);  // deletes itself,

  // When the last instance has been deleted, Hello_App calls Gtk::Main::quit(), causing run() to return:
  gnomeMain.run();

  return 0;
}

// gnomehello-save-session
bool Gnome_Hello::save_session(gint phase, GnomeSaveStyle save_style,
			  bool is_shutdown, GnomeInteractStyle interact_style,
			  bool is_fast)
{
  std::cout << "save_session" << std::endl;
  std::vector<std::string> argv(4);

  if (!options.message_.empty())
  {
    argv.push_back("--message");
    argv.push_back(options.message_);
  }
  
  client_->set_clone_command(argv);
  client_->set_restart_command(argv);

  return true;
}


// gnomehello-session-die */
void
Gnome_Hello::session_die(gpointer client_data)
{
  std::cout << "session_die" << std::endl;
}
