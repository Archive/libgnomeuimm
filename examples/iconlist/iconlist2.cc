#include <string.h>
#include <libgnomeuimm.h>

static GtkTargetEntry target_table[] = {
  {"someicon", 1, 2},
};


class TestIconList : public Gnome::UI::IconList
{
public:
    TestIconList();

protected:
    virtual void on_select_icon(int icon, GdkEvent* event);
    virtual void on_unselect_icon(int icon, GdkEvent* event);
    virtual void on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& context,
                                     Gtk::SelectionData& data,
                                     guint info,
                                     guint time);
    virtual void on_drag_data_delete(const Glib::RefPtr<Gdk::DragContext>& context);

private:
    int m_selected;
};


TestIconList::TestIconList()
    : m_selected(-1)
{
    int position = append("icon1.png", "Icon1");
    set_icon_data(position,(gpointer) "Icon1");
    position = append("icon2.png", "Icon2");
    set_icon_data(position, (gpointer) "Icon2");
}


void 
TestIconList::on_select_icon(int icon, GdkEvent* e)
{
    Gnome::UI::IconList::on_select_icon(icon, e);
    if(m_selected != icon) {
        drag_source_set( target_table, Gdk::BUTTON1_MASK, Gdk::ACTION_COPY  | Gdk::ACTION_MOVE);
        m_selected = icon;
        
        // send event again to start drag
        // (how can this be done in a better way?)
        event(e);
    }
}


void 
TestIconList::on_unselect_icon(int icon, GdkEvent* event)
{
    Gnome::UI::IconList::on_unselect_icon(icon, event);
    drag_source_unset();
    m_selected = -1;
}


void 
TestIconList::on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& context,
                                  Gtk::SelectionData& data,
                                  guint info,
                                  guint time)
{
    Gnome::UI::IconList::on_drag_data_get(context, data, info, time);
    char* name = (char*) get_icon_data(m_selected);
    data.set(data.get_target(),
             8, 
             (const guchar*) name,
             strlen(name));
}


void 
TestIconList::on_drag_data_delete(const Glib::RefPtr<Gdk::DragContext>& context)
{
    // when will this method be called?
    Gnome::UI::IconList::on_drag_data_delete(context);
    std::cerr << "TestIconList::on_drag_data_delete called" << std::endl;
}


class IconWindow :  public Gtk::Window
{
public:
    IconWindow();

protected:

    void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,
                           int x,
                           int y,
                           const Gtk::SelectionData& data,
                           guint info,
                           guint time);

    Gtk::VBox m_vbox;
    TestIconList m_iconlist;
    Gtk::Label m_label;
};


IconWindow::IconWindow()
  : m_label("Drop icon here!")
{
    set_title("Iconlist Example");
    set_default_size(300, 300);

    m_vbox.pack_start(m_iconlist);
    m_vbox.pack_start(m_label, Gtk::PACK_SHRINK);
    add(m_vbox);

    m_label.drag_dest_set(target_table, Gtk::DEST_DEFAULT_ALL, Gdk::ACTION_COPY | Gdk::ACTION_MOVE);
    m_label.signal_drag_data_received().connect(sigc::mem_fun(*this, &IconWindow::on_drag_data_received));
    
    show_all();
}


void
IconWindow::on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,
                              int x,
                              int y,
                              const Gtk::SelectionData& data,
                              guint info,
                              guint time)
{
    std::cerr << "You gave me icon '" << data.get_data_as_string() << "'" << std::endl;
}


int 
main(int argc, char *argv[])
{
   Gnome::Main kit("Iconlist", "0.0.0", Gnome::UI::module_info_get(), argc, argv);

   IconWindow window;
   
   kit.run(window);

   return 0;
}

