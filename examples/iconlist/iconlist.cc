#include <libgnomeuimm.h>


class TestIconList : public Gnome::UI::IconList
{
public:
    TestIconList ();

protected:
    virtual void on_select_icon (int icon, GdkEvent* event);
    virtual void on_unselect_icon (int icon, GdkEvent* event);
};


TestIconList::TestIconList ()
{
    set_selection_mode (Gtk::SELECTION_EXTENDED);
    set_separators ("icon1.png");

    append ("icon1.png", "Icon1");
    append ("icon2.png", "Icon2");
}


void 
TestIconList::on_select_icon (int icon, GdkEvent* event)
{
    Gnome::UI::IconList::on_select_icon (icon, event);
    std::cerr << "icon select: " << icon << std::endl;
}


void 
TestIconList::on_unselect_icon (int icon, GdkEvent* event)
{
    Gnome::UI::IconList::on_unselect_icon (icon, event);
    std::cerr << "icon unselect: " << icon << std::endl;
}


class IconWindow : public Gtk::Window
{
public:
    IconWindow ();

protected:
    TestIconList m_iconlist;
};


IconWindow::IconWindow ()
{
    set_title ("Iconlist Example");
    set_default_size (300, 300);

    add (m_iconlist);

    show_all_children ();
}
 
main (int argc, char *argv[])
{
   Gnome::Main kit ("Iconlist", "0.0.0", Gnome::UI::module_info_get(), argc, argv);

   IconWindow window;
   
   kit.run (window);

   return 0;
}

