#include <libgnomeuimm.h>

// Demonstration of how to create menu templates using arrays

using sigc::mem_fun;
using sigc::bind;


class MyApp : public Gnome::UI::App
{
public:
  MyApp();
};

MyApp::MyApp()
: Gnome::UI::App("MyApp", "MyApp")
{
  using namespace Gnome::UI;

  Items::Info file_menu[] =
  {
    MenuItems::Open(),
    MenuItems::Save(),
    MenuItems::SaveAs(),
    Items::Separator(),
    MenuItems::Close(),
    MenuItems::Exit()
  };

  Items::Info edit_menu[] =
  {
    MenuItems::Cut(),
    MenuItems::Copy(),
    MenuItems::Paste(),
    MenuItems::SelectAll(),
    MenuItems::Clear(),
    MenuItems::Undo(),
    MenuItems::Redo(),
    MenuItems::Find(),
    MenuItems::FindAgain(),
    MenuItems::Replace(),
    MenuItems::Properties()
  };

  Items::Info help_menu[] =
  {
    MenuItems::About()
  };

  Items::SubTree menus[]=
  {
    Menus::File(file_menu),
    Menus::Edit(edit_menu),
    Menus::Help(help_menu)
  };

  create_menus(menus);

  set_statusbar(*manage(new Gtk::Statusbar));
  install_menu_hints();

  set_default_size(400, 300);
  //set_policy(false, true, false);

  show();
}

int main (int argc, char* argv[])
{
  Gnome::Main kit("myapp-demo", "0.0", Gnome::UI::module_info_get(), argc, argv);
  MyApp app;
  kit.run(app);
  return 0;
}


