// -*- Mode: C++; c-basic-offset: 2 -*-

#include <libgnomemm/main.h>
#include <libgnomeuimm/init.h>
#include <libgnomeuimm/app-helper.h>
#include <libgnomeuimm/ui-items-stock.h>

#include <gtkmm/window.h>
#include <gtkmm/menu.h>
#include <gtkmm/label.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/stock.h>

#include <sigc++/bind_return.h>

// Demonstration of how to create a popup menu that contains stock
// menu items

class MyApp : public Gtk::Window
{
  Gtk::Menu popup_menu;
public:
  MyApp();

  void click_cb(GdkEventButton* e);
};

#include <iostream>

void paste_cb()
{
  std::cerr << "Paste" << std::endl;
}

MyApp::MyApp()
{
  // Create list of stock menu items
  Gnome::UI::Items::Info popup_menu_items[] =
  {
    Gnome::UI::MenuItems::Cut(),
    Gnome::UI::MenuItems::Copy(),
    Gnome::UI::MenuItems::Paste(sigc::ptr_fun(&paste_cb)),
    Gnome::UI::Items::Separator(),
    Gnome::UI::Items::Item(Gnome::UI::Items::Icon(Gtk::Stock::HOME), "Custom item"),
  };
  
  Gnome::UI::Items::Array<Gnome::UI::Items::Info> popup_menu_items_filled =
    Gnome::UI::Items::fill(popup_menu,
			   popup_menu_items,
			   popup_menu.get_accel_group());

  // Disable Cut & Paste
  popup_menu_items_filled[0].get_widget()->set_sensitive(false); // Cut
  popup_menu_items_filled[1].get_widget()->set_sensitive(false); // Paste
  
  Gtk::EventBox* event_box = new Gtk::EventBox;
  event_box->add(*manage(new Gtk::Label("Right-click me")));
  event_box->signal_button_press_event().connect(
    sigc::bind_return( sigc::mem_fun(*this, &MyApp::click_cb), false) );

  add(*manage(event_box));
  set_default_size(400, 300);

  show_all();
}

void MyApp::click_cb(GdkEventButton* e)
{
  if(e->button == 3)
    popup_menu.popup(e->button, e->time);
}

int main(int argc, char* argv[])
{
  Gnome::Main kit("popup-demo", "0.0", Gnome::UI::module_info_get(), argc, argv);
  MyApp app;
  kit.run(app);
  return 0;
}


