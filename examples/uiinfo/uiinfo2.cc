#include <libgnomeuimm.h>
#include <vector>
#include <list>

// Demonstration of creating menu templates with STL types

using sigc::mem_fun;
using sigc::bind;


class MyApp : public Gnome::UI::App
{
public:
  MyApp();
};

MyApp::MyApp()
: Gnome::UI::App("MyApp", "MyApp")
{
  using namespace Gnome::UI;

  // we can use vectors
  std::vector<Items::Info> file_menu;
  file_menu.push_back(MenuItems::Open());
  file_menu.push_back(MenuItems::Save());
  file_menu.push_back(MenuItems::SaveAs());
  file_menu.push_back(Items::Separator());
  file_menu.push_back(MenuItems::Close());
  file_menu.push_back(MenuItems::Exit());

  // or we can use lists
  std::list<Items::Info> edit_menu;
  edit_menu.push_back(MenuItems::Cut());
  edit_menu.push_back(MenuItems::Copy());
  edit_menu.push_back(MenuItems::Paste());
  edit_menu.push_back(MenuItems::SelectAll());
  edit_menu.push_back(MenuItems::Clear());
  edit_menu.push_back(MenuItems::Undo());
  edit_menu.push_back(MenuItems::Redo());
  edit_menu.push_back(MenuItems::Find());
  edit_menu.push_back(MenuItems::FindAgain());
  edit_menu.push_back(MenuItems::Replace());
  edit_menu.push_back(MenuItems::Properties());

  std::vector<Items::Info> help_menu;
  help_menu.push_back(MenuItems::About());

  std::vector<Items::SubTree> menus;
  menus.push_back(Menus::File(file_menu));
  menus.push_back(Menus::Edit(edit_menu));
  menus.push_back(Menus::Help(help_menu));

  create_menus(menus);

  set_statusbar(*manage(new Gtk::Statusbar));
  install_menu_hints();

  set_default_size(400, 300);
  //set_policy(false, true, false);

  show();
}

int main (int argc, char* argv[])
{
  Gnome::Main kit("myapp-demo", "0.0", Gnome::UI::module_info_get(), argc, argv);
  MyApp app;
  kit.run(app);
  return 0;
}


