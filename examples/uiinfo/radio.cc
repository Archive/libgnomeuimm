#include <libgnomeuimm.h>
#include <iostream>

using sigc::mem_fun;
using sigc::bind;

class MyApp : public Gnome::UI::App
{
  public:
    MyApp();
};

void foo(int i)
{
  std::cout << i << std::endl;
}

void foo2()
{
  std::cout << "yadda yadda" << std::endl;
}

MyApp::MyApp() : Gnome::UI::App("MyApp","MyApp")
{
  using namespace Gnome::UI;

  Items::Info radioitems[] =
  {
    Items::Item("Item1", sigc::bind(sigc::ptr_fun(&foo), 1)),
    Items::Item("Item2", sigc::bind(sigc::ptr_fun(&foo), 2)),
    Items::Item("Item3", sigc::bind(sigc::ptr_fun(&foo), 3)),
    Items::Item("Item4", sigc::bind(sigc::ptr_fun(&foo), 4))
  };

  Items::Info radioitems2[] =
  {
    Items::Item("Item1", sigc::ptr_fun(&foo2)),
    Items::Item("Item2", sigc::ptr_fun(&foo2))
  };

  Items::Info radio_menu[] =
  {
    Items::Item("Not Radio"),
    Items::Separator(),
    Items::RadioTree(radioitems),
    Items::Separator(),
    Items::RadioTree(radioitems2)
  };

  Items::SubTree menus[]=
  {
    Items::Menu("Items", radio_menu)
  };

  create_menus(menus);
  show();
}

int main (int argc, char* argv[])
{
  Gnome::Main kit("radio-info-demo", "0.0", Gnome::UI::module_info_get(), argc, argv);
  MyApp app;
  kit.run(app);
  return 0;
}


