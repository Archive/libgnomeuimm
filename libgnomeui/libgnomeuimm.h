/* $Id$ */
/* libgnomeuimm - a C++ wrapper for libgnomeui
 *
 * Copyright 1999-2001 Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef LIBGNOMEUIMM_H
#define LIBGNOMEUIMM_H

//extern "C" {
//#include <gtk/gtk.h>
//}

/* libgnomeuimm version.  */
extern const int libgnomeuimm_major_version;
extern const int libgnomeuimm_minor_version;
extern const int libgnomeuimm_micro_version;

#include <libgnomemm.h>
#include <gconfmm.h>

#include <libgnomeuimm/about.h>
#include <libgnomeuimm/app.h>
#include <libgnomeuimm/appbar.h>
#include <libgnomeuimm/client.h>
#include <libgnomeuimm/color-picker.h>
#include <libgnomeuimm/dateedit.h>
#include <libgnomeuimm/druid.h>
#include <libgnomeuimm/druid-page.h>
#include <libgnomeuimm/druid-page-edge.h>
#include <libgnomeuimm/druid-page-standard.h>
#include <libgnomeuimm/entry.h>
#include <libgnomeuimm/file-entry.h>
#include <libgnomeuimm/font-picker.h>
#include <libgnomeuimm/href.h>
#include <libgnomeuimm/icon-entry.h>
#include <libgnomeuimm/icon-list.h>
#include <libgnomeuimm/icon-sel.h>
#include <libgnomeuimm/init.h>
#include <libgnomeuimm/pixmap-entry.h>
#include <libgnomeuimm/stock.h>
#include <libgnomeuimm/thumbnail.h>
#include <libgnomeuimm/icon-lookup.h>


#endif /* #ifndef LIBGNOMEUIMM_H */
