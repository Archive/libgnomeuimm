/* 
 * Copyright 2000 Karl Nelson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <string.h>
#include <gtk/gtk.h>
#include <libgnomeuimm/app-helper.h>
#include <glibmm/exceptionhandler.h>
#include <cstring>


namespace Gnome
{

namespace UI
{

namespace Items
{


extern "C"
{

static void libgnomeuimm_info_call(void* object, void* data)
{
  try
  {
    GtkWidget* cWidget = (GtkWidget*)object; //but we ignore this.
    InfoData* d = (InfoData*) data;
    d->callback_();
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }
}


} // "C"




/*******************************************************************/
Info::~Info()
{
  if(get_data_())
    get_data_()->unref();
}

Info::Info()
{
  GnomeUIInfo::type = GnomeUIInfoType(0);
  GnomeUIInfo::label = 0;
  GnomeUIInfo::hint = 0;
  GnomeUIInfo::moreinfo = 0;
  GnomeUIInfo::user_data = 0;
  GnomeUIInfo::unused_data = 0;
  GnomeUIInfo::pixmap_type = GnomeUIPixmapType(0);
  GnomeUIInfo::pixmap_info = 0;
  GnomeUIInfo::accelerator_key = 0;
  GnomeUIInfo::ac_mods = GdkModifierType(0);
  GnomeUIInfo::widget = 0;
}

Info::Info(const Info& src)
{
  //Copy the struct data all at once:
  memcpy(this, &src, sizeof(Info));


  if(get_data_())
    get_data_()->ref();
}

Info& Info::operator=(const Info& src)
{
  //Don't copy it into itself:
  if(this == &src)
    return *this;

  if(get_data_())
    get_data_()->unref();

  //Copy the struct data all at once:
  memcpy(this, &src, sizeof(Info));


  if(get_data_())
    get_data_()->ref();

  return *this;
}

Info::Type Info::type() const
{
  return Type(GnomeUIInfo::type);
}

InfoData* Info::get_data_()
{
  return static_cast<InfoData*>(GnomeUIInfo::unused_data);
}

const InfoData* Info::get_data_() const
{
  return static_cast<InfoData*>(GnomeUIInfo::unused_data);
}

void Info::set_data_(InfoData* infodata)
{
  GnomeUIInfo::unused_data = (void*)infodata;
}

Gtk::Widget* Info::get_widget()
{
  return Glib::wrap(GnomeUIInfo::widget);
}

const Gtk::Widget* Info::get_widget() const
{
  return Glib::wrap(GnomeUIInfo::widget);
}


void Info::set_accel(const Gtk::AccelKey& key)
{
  GnomeUIInfo::accelerator_key = key.get_key();
  GnomeUIInfo::ac_mods = GdkModifierType(key.get_mod());
  if (accelerator_key == GDK_VoidSymbol)
  {
    accelerator_key = 0;
    GnomeUIInfo::ac_mods = GdkModifierType(0);
  }
}

void Info::init(Type type_)
{
  GnomeUIInfo::type = GnomeUIInfoType(type_);
}

void Info::init_cb(Type type_, const Icon& icon_,
     const Glib::ustring& label_, const Callback& callback_,
     const Glib::ustring& hint_)
{  
  InfoData* info_ = init_common(type_, icon_, label_, hint_);
  info_->set_callback(callback_);

  info_->connect(*this);
}

InfoData* Info::init_common(Type type_, const Icon& icon_,
                       const Glib::ustring& label_, const Glib::ustring& hint_)
{
  GnomeUIInfo::type = GnomeUIInfoType(type_);
  set_accel();
  
  return new InfoData(label_, hint_, icon_);
}


void Info::init_sub(Type type_,const Icon& icon_,
          const Glib::ustring& label_, const Array<Info>& array,
          const Glib::ustring& hint_)
{
  InfoData* info_ = init_common(type_, icon_, label_, hint_);
  info_->set_subtree(array);

  info_->connect(*this);
}

const gchar* Info::debug_get_icon_info() const
{
  return (const char*)get_data_()->icon_.get_pixmap_info();
}


SubTree::SubTree()
{
}


SubTree::SubTree(const Glib::ustring& label,const Array<Info>& uitree,
                 const Glib::ustring& tip)
{
  init_sub(SUBTREE, Icon(), label, uitree, tip);
}

SubTree::SubTree(const Icon& icon, const Glib::ustring& label,
               const Array<Info>& uitree, const Glib::ustring& tip)
{
  init_sub(SUBTREE, icon, label, uitree, tip);
}

SubTree::~SubTree()
{
}

Begin::Begin()
{
  init(BUILDER);
  GnomeUIInfo::moreinfo = &build_data_;
}


End::End()
{
  init(END);
}
  


extern "C"
{

static void
libgnomeuimm_info_connect(GnomeUIInfo* uiinfo, const char* signal_name, GnomeUIBuilderData* uibdata)
{
  if(uiinfo->moreinfo)
  {
    g_signal_connect_data(G_OBJECT(uiinfo->widget), signal_name, (GCallback)(uiinfo->moreinfo),
                          (gpointer)(uiinfo->user_data), 0, (GConnectFlags)0);
  }
}

} // "C"


//libgnomeui will call libgnomeuimm_info_connect() to connect item signals:
GnomeUIBuilderData Begin::build_data_ = { &libgnomeuimm_info_connect, 0, 0, 0, 0 };

/*******************************************************************/


} /* namespace Items */




//Gtk::MenuShell* MenuShell_find_menu_pos(const Gtk::MenuShell& parent,
//                                               const Glib::ustring &path,
//                                               gint& pos)
//{
  //gnome_app_find_menu_pos() actually wants a GtkMenuShell*, but the arg is a GtkWidget*.
//  GtkWidget* pGtkMenuShell = gnome_app_find_menu_pos(GTK_WIDGET(parent.gobj()), path.c_str(), &pos);
//  return Gtk::wrap(GTK_MENU_SHELL(pGtkMenuShell));
//}

namespace
{

struct UIArrayHolder
{
   Items::Array<Items::Info> info_;
   UIArrayHolder(const Items::Array<Items::Info>& info): info_(info) {}

   static void destroy(void* d)
   {
     delete ((UIArrayHolder*)d);
   }
};

} // namespace


namespace Items {

Array<Info> fill (Gtk::MenuShell                      &menu_shell,
		  const Array<Info>                   &info,
		  const Glib::RefPtr<Gtk::AccelGroup> &accel_group,
		  bool                                 uline_accels,
		  int                                  pos)
{
  menu_shell.set_data("gnomemm-uihold", new UIArrayHolder(info), UIArrayHolder::destroy);
  gnome_app_fill_menu(menu_shell.gobj(), info.gobj(),
		      accel_group ? accel_group->gobj() : 0,
		      (gboolean)uline_accels, pos);

  return info;
}

Array<Info> fill (Gtk::Toolbar                        &toolbar,
		  const Array<Info>                   &info,
		  const Glib::RefPtr<Gtk::AccelGroup> &accel_group)
{
  toolbar.set_data("gnomemm-uihold", new UIArrayHolder(info), UIArrayHolder::destroy);
  gnome_app_fill_toolbar(toolbar.gobj(), info.gobj(),
			 accel_group ? accel_group->gobj() : 0);

  return info;
}




InfoData::InfoData()
: callback_(), subtree_()
{
  ref_count_ = 1;
}

InfoData::InfoData(const Glib::ustring& label, const Glib::ustring& hint, const Icon& icon)
: callback_(), subtree_(), label_(label), hint_(hint), icon_(icon)
{
  ref_count_ = 1;
}

InfoData::~InfoData()
{
}

void InfoData::ref()
{
  ref_count_++;
}

void InfoData::unref()
{
  if (!--ref_count_)
    delete this;
}

void InfoData::connect(Info& info)
{
  //Fill the GnomeUIInfo with the data from this InfoData.
  //This data will live as long as the GnomeUIInfo, because InfoData is shared and reference-counted.
  info.label = ( label_.empty() ? 0 : label_.c_str());
  info.hint = ( hint_.empty() ? 0 : hint_.c_str());

  //Icon:
  info.pixmap_type = (GnomeUIPixmapType)icon_.get_type();
  info.pixmap_info = icon_.get_pixmap_info();

  info.set_data_(this); //Store this so that it can be reference-counted.
  
  if(callback_) //This was if(callback_.connected()) for libsigc++ 1.2, and I'm not sure about the change. murrayc.
  {
    info.user_data = this;
    info.moreinfo = (void*)&libgnomeuimm_info_call;
  }
  
  if(info.type() == Info::SUBTREE)
  {
    info.moreinfo = (void*)subtree_.gobj();
  }
  else if(info.type() == Info::HELP) // Dirty hack
  {
    info.moreinfo = (void*) info.label;
    info.label = 0;
  }
}

/* This is a bit of a kludge to get around some problems with the
   Items::Info structure.  Many of the actions on Items::Info imply the
   availablity of the Widget - something which as a rule does not
   happen in gtkmm.  That is, we wash the object because most often
   it isn't relevant.  Items::Info is different - since the user never
   constructed the object they don't know its address.

   The solution is to allow both gtk+ type slots and gtkmm style
   slots with and without the object.  To do this it is wired such
   that InfoData only stores the with-object style.  The following converter
   changes a with to a without.

   All info methods now take both slot styles.
*/

void InfoData::set_callback(const Info::Callback& callback)
{
  //if(callback_) //TODO: This was if(callback_.connected()) for libsigc++ 1.2, and I'm not sure about the change. murrayc.
    callback_ = callback;
}

void InfoData::set_subtree(const Array<Info>& subtree)
{
  subtree_ = subtree;
}

Array<Info>& InfoData::get_subtree()
{
  return subtree_; //Removes Begin and End.
}


} /* namespace Items */

} /*namespace UI */

} /* namespace Gnome */

