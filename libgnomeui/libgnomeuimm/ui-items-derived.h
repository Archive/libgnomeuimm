/*
 * Copyright 2000-2002 The libgnomeuimm development team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */


#ifndef LIBGNOMEUIMM_UI_ITEMS_DERIVED_H
#define LIBGNOMEUIMM_UI_ITEMS_DERIVED_H

#include <libgnomeuimm/app-helper.h>

namespace Gnome
{

namespace UI
{

namespace Items
{

class Separator : public Info
{
public:
  Separator();
  ~Separator();
  operator Gtk::Menu_Helpers::Element();
};


///Represents menu items and toolbar items
class Item : public Info
{
protected:
  Item();
public:
  Item(const Icon& icon, const Glib::ustring& label,
       const Callback& cb = Callback(), const Glib::ustring& tip = Glib::ustring());
  Item(const Glib::ustring& label, const Callback& cb = Callback(), const Glib::ustring& tip = Glib::ustring());

  ~Item();
};

///Represents toggle items and toggle buttons
class ToggleItem : public Info
{
public:
  ToggleItem(const Icon& icon, const Glib::ustring& label,
             const Callback& cb = Callback(), const Glib::ustring& tip = Glib::ustring());
  ToggleItem(const Glib::ustring& label, const Callback& cb = Callback(),
             const Glib::ustring& tip = Glib::ustring());
  ~ToggleItem();
};

/** Loads <prefix>/share/gnome/help/<app_name>/<locale>/topic.dat
 * and creates menu items for each section
 */
class Help : public Info
{
public:
  Help(const Glib::ustring& app_name);
  ~Help();
};


// this tree can only hold Items.
class RadioTree : public Info
{
public:
  RadioTree(const Array<Info>& array);
  ~RadioTree();
private:
  static GnomeUIBuilderData build_data_;
};

} // namespace Items
} // namespace UI
} // namespace Gnome

#endif //LIBGNOMEUIMM_UI_ITEMS_DERIVED_H
