/*
 * Copyright 2000-2002 The libgnomeuimm development team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <libgnomeuimm/ui-items-icon.h>

namespace Gnome
{

namespace UI
{

namespace Items
{
  
//Icon:

Icon::Icon(const Gtk::StockID& stock_id)
: pixmap_type_(STOCK), stock_id_(stock_id.get_string()), 
  xpm_data_(0)
{
 
}


Icon::Icon(Type type)
: pixmap_type_(type), xpm_data_(0)
{}


Icon::~Icon()
{}

Icon::Type Icon::get_type() const
{
    return pixmap_type_;
}

gconstpointer Icon::get_pixmap_info() const
{
  if(xpm_data_)
    return xpm_data_;
  else if(!stock_id_.empty())
    return stock_id_.c_str();
  else if(!filename_.empty())
    return filename_.c_str();
  return NULL;
}

//IconXpm:

IconXpm::IconXpm(xpmdata_t xpm)
: Icon(DATA)
{
  xpm_data_ = xpm;
}

IconXpm::~IconXpm()
{}

//IconFile:

IconFile::IconFile(const std::string& file)
: Icon(FILENAME)
{
  filename_ = file;  
}

IconFile::~IconFile()
{}

} // namespace Items
} // namespace UI
} // namespace Gnome

