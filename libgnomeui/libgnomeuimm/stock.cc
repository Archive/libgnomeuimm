// -*- c++ -*-
/* $Id$ */

/* Copyright (C) 2002 The gnomemm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomeuimm/stock.h>
#include <gtkmm/stock.h>
#include <gtkmm/stockitem.h>
#include <libgnomeui/gnome-stock-icons.h>


namespace Gnome
{

namespace UI
{

namespace Stock
{

const Gtk::BuiltinStockID DIALOG_INFO = { GTK_STOCK_DIALOG_INFO };

const Gtk::BuiltinStockID TIMER = { GNOME_STOCK_TIMER };
const Gtk::BuiltinStockID TIMER_STOP = { GNOME_STOCK_TIMER_STOP };
const Gtk::BuiltinStockID TRASH = { GNOME_STOCK_TRASH };
const Gtk::BuiltinStockID TRASH_FULL = { GNOME_STOCK_TRASH_FULL };

const Gtk::BuiltinStockID SCORES = { GNOME_STOCK_SCORES };
const Gtk::BuiltinStockID ABOUT = { GNOME_STOCK_ABOUT };
const Gtk::BuiltinStockID BLANK = { GNOME_STOCK_BLANK };

const Gtk::BuiltinStockID VOLUME = { GNOME_STOCK_VOLUME };
const Gtk::BuiltinStockID MIDI = { GNOME_STOCK_MIDI };
const Gtk::BuiltinStockID MIC = { GNOME_STOCK_MIC };
const Gtk::BuiltinStockID LINE_IN = { GNOME_STOCK_LINE_IN };

const Gtk::BuiltinStockID MAIL = { GNOME_STOCK_MAIL };
const Gtk::BuiltinStockID MAIL_RCV = { GNOME_STOCK_MAIL_RCV };
const Gtk::BuiltinStockID MAIL_SND = { GNOME_STOCK_MAIL_SND };
const Gtk::BuiltinStockID MAIL_RPL = { GNOME_STOCK_MAIL_RPL };
const Gtk::BuiltinStockID MAIL_FWD = { GNOME_STOCK_MAIL_FWD };
const Gtk::BuiltinStockID MAIL_NEW = { GNOME_STOCK_MAIL_NEW };
const Gtk::BuiltinStockID ATTACH = { GNOME_STOCK_ATTACH };

const Gtk::BuiltinStockID BOOK_RED = { GNOME_STOCK_BOOK_RED };
const Gtk::BuiltinStockID BOOK_GREEN = { GNOME_STOCK_BOOK_GREEN };
const Gtk::BuiltinStockID BOOK_BLUE = { GNOME_STOCK_BOOK_BLUE };
const Gtk::BuiltinStockID BOOK_YELLOW = { GNOME_STOCK_BOOK_YELLOW };
const Gtk::BuiltinStockID BOOK_OPEN = { GNOME_STOCK_BOOK_OPEN };

const Gtk::BuiltinStockID MULTIPLE_FILE = { GNOME_STOCK_MULTIPLE_FILE };
const Gtk::BuiltinStockID NOT = { GNOME_STOCK_NOT };

const Gtk::BuiltinStockID TABLE_BORDERS = { GNOME_STOCK_TABLE_BORDERS };
const Gtk::BuiltinStockID TABLE_FILL = { GNOME_STOCK_TABLE_FILL };

const Gtk::BuiltinStockID TEXT_INDENT = { GNOME_STOCK_TEXT_INDENT };
const Gtk::BuiltinStockID TEXT_UNINDENT = { GNOME_STOCK_TEXT_UNINDENT };
const Gtk::BuiltinStockID TEXT_BULLETED_LIST = { GNOME_STOCK_TEXT_BULLETED_LIST };
const Gtk::BuiltinStockID TEXT_NUMBERED_LIST = { GNOME_STOCK_TEXT_NUMBERED_LIST };

} //namespace Stock

} //namespace UI

} //namespace Gnome
