/*
 * Copyright 2000-2002 The libgnomeuimm development team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <libgnomeuimm/ui-items-derived.h>
#include <gtk/gtk.h>

namespace Gnome
{

namespace UI                  
{

namespace Items
{

Separator::Separator()
{
  init(SEPARATOR);
}

Separator::~Separator()
{}

  
Separator::operator Gtk::Menu_Helpers::Element()
{
  Gtk::MenuItem* item = manage(new Gtk::MenuItem());
  item->show();
  return *item;
}


Item::Item()
{}

Item::Item(const Icon& icon, const Glib::ustring& label,
           const Callback& cb, const Glib::ustring& tip)
{
  // g_warning("Item::Item(): icon=%10X, icon=%s", &icon, icon.get_pixmap_info());
  init_cb(ITEM, icon, label, cb, tip);
  //g_warning("Item::Item(): %s", pixmap_info);
}

Item::Item(const Glib::ustring& label, const Callback& cb, const Glib::ustring& tip)
{
  init_cb(ITEM, Icon(), label, cb, tip);
}

Item::~Item()
{}
  

ToggleItem::ToggleItem(const Icon& icon, const Glib::ustring& label,
                       const Callback& cb, const Glib::ustring& tip)
{
  init_cb(TOGGLEITEM, icon, label, cb, tip);
}

ToggleItem::ToggleItem(const Glib::ustring& label, const Callback& cb, const Glib::ustring& tip)
{
  init_cb(TOGGLEITEM, Icon(), label, cb, tip);
}

ToggleItem::~ToggleItem()
{}


Help::Help(const Glib::ustring& app_name)
{
  //init_cb(HELP, Icon(), 0, 0, Glib::ustring());
  GnomeUIInfo::type = GnomeUIInfoType(HELP);

  //The .dat identifier:
  GnomeUIInfo::moreinfo = (void*)app_name.c_str(); //const char* Should probably live as long as the app.
}

Help::~Help()
{}
  

  

  



Array<Info>& SubTree::get_uitree()
{
  InfoData* pInfoData = get_data_();
  return pInfoData->get_subtree();
}

extern "C"
{

static void
libgnomeui_callback_info(GtkWidget* widget, gpointer data)
{
  InfoData* infodata = (InfoData*)data;

  if(infodata->callback_) //If a sigc::slot has been connected.
  {
    if(GTK_IS_TOGGLE_BUTTON(widget) && GTK_TOGGLE_BUTTON(widget)->active)
    {
      //Call the sigc::slot:
      infodata->callback_();
    }
    else if(GTK_IS_CHECK_MENU_ITEM(widget) && GTK_CHECK_MENU_ITEM(widget)->active)
    {
      //Call the sigc::slot:
      infodata->callback_();
    }
  }
}

static void
libgnomeuimm_radio_info_connect(GnomeUIInfo* uiinfo, const gchar* signal_name, GnomeUIBuilderData* uibdata)
{
  g_signal_connect_data(G_OBJECT (uiinfo->widget), signal_name, GCallback(libgnomeui_callback_info),
                       (gpointer)(uiinfo->user_data), 0, (GConnectFlags)0);
}

} // extern "C"


//libgnomeui will call libgnomeuimm_radio_info_connect() to connect item signals:
GnomeUIBuilderData RadioTree::build_data_ = { &libgnomeuimm_radio_info_connect, 0, 0, 0, 0 };

RadioTree::RadioTree(const Items::Array<Info>& array)
{
  GnomeUIInfo::type = GnomeUIInfoType(RADIOITEMS);
  Items::InfoData* info_ = new Items::InfoData();
  info_->set_subtree(array);
  info_->subtree_.gobj()->moreinfo = (void*)&build_data_;
  moreinfo = (void*)info_->subtree_.gobj();
}

RadioTree::~RadioTree()
{}



} // namespace Items
} // namespace UI
} // namespace Gnome
