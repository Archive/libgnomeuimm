/*
 * Copyright 2000-2002 The libgnomeuimm development team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */


#ifndef LIBGNOMEUIMM_UI_ITEMS_STOCK_H
#define LIBGNOMEUIMM_UI_ITEMS_STOCK_H

#include <libgnomeuimm/ui-items-derived.h>

namespace Gnome
{

namespace UI
{

namespace Items
{
  
// configurable item (base for stock)
class ConfigureItem: public Item
{
public:
  ~ConfigureItem() {}
protected:
  ConfigureItem() {}
  void init(const Callback &cv, GnomeUIInfoConfigurableTypes ct);
  void init(const Callback &cv, GnomeUIInfoConfigurableTypes ct, const Glib::ustring& strLabel, const Glib::ustring& strHint);
};

} // namespace Items



// Stock MenuIrems
//   These all operate both a menu element for gtkmm and as Items::Info for
//   libgnomeuimm.  They must be static or in a container and thus can not
//   be newed.
namespace MenuItems
{

// Macro to create stock menu item:
#define STOCK_MENU_ITEM(X, Y) \
  struct X : public Items::ConfigureItem \
  { \
    X(const Callback& cb = Callback()) \
    { \
     init(cb, Y); \
    } \
    ~X() {} \
  }

// To create stock menu items that need label and hint (e.g. 'New'):
#define STOCK_MENU_ITEM_EXTRA(X, Y) \
  struct X : public Items::ConfigureItem \
  { \
    X(const Glib::ustring& strLabel, const Glib::ustring& strHint, const Callback& cb = Callback()) \
    { \
     init(cb, Y, strLabel, strHint); \
    } \
    ~X() {} \
  }

/* File Menu */
STOCK_MENU_ITEM_EXTRA(New,GNOME_APP_CONFIGURABLE_ITEM_NEW); //Needs label and tip

STOCK_MENU_ITEM(Open,GNOME_APP_CONFIGURABLE_ITEM_OPEN);
STOCK_MENU_ITEM(Save,GNOME_APP_CONFIGURABLE_ITEM_SAVE);
STOCK_MENU_ITEM(SaveAs,GNOME_APP_CONFIGURABLE_ITEM_SAVE_AS);
STOCK_MENU_ITEM(Revert,GNOME_APP_CONFIGURABLE_ITEM_REVERT);
STOCK_MENU_ITEM(Print,GNOME_APP_CONFIGURABLE_ITEM_PRINT);
STOCK_MENU_ITEM(PrintSetup,GNOME_APP_CONFIGURABLE_ITEM_PRINT_SETUP);
STOCK_MENU_ITEM(Close,GNOME_APP_CONFIGURABLE_ITEM_CLOSE);
STOCK_MENU_ITEM(Exit,GNOME_APP_CONFIGURABLE_ITEM_EXIT);

/* Edit menu */
STOCK_MENU_ITEM(Cut,GNOME_APP_CONFIGURABLE_ITEM_CUT);
STOCK_MENU_ITEM(Copy,GNOME_APP_CONFIGURABLE_ITEM_COPY);
STOCK_MENU_ITEM(Paste,GNOME_APP_CONFIGURABLE_ITEM_PASTE);
STOCK_MENU_ITEM(SelectAll,GNOME_APP_CONFIGURABLE_ITEM_SELECT_ALL);
STOCK_MENU_ITEM(Clear,GNOME_APP_CONFIGURABLE_ITEM_CLEAR);
STOCK_MENU_ITEM(Undo,GNOME_APP_CONFIGURABLE_ITEM_UNDO);
STOCK_MENU_ITEM(Redo,GNOME_APP_CONFIGURABLE_ITEM_REDO);
STOCK_MENU_ITEM(Find,GNOME_APP_CONFIGURABLE_ITEM_FIND);
STOCK_MENU_ITEM(FindAgain,GNOME_APP_CONFIGURABLE_ITEM_FIND_AGAIN);
STOCK_MENU_ITEM(Replace,GNOME_APP_CONFIGURABLE_ITEM_REPLACE);
STOCK_MENU_ITEM(Properties,GNOME_APP_CONFIGURABLE_ITEM_PROPERTIES);

/* Settings menu */
STOCK_MENU_ITEM(Preferences,GNOME_APP_CONFIGURABLE_ITEM_PREFERENCES);

/* Windows menu */
STOCK_MENU_ITEM(NewWindow,GNOME_APP_CONFIGURABLE_ITEM_NEW_WINDOW);
STOCK_MENU_ITEM(CloseWindow,GNOME_APP_CONFIGURABLE_ITEM_CLOSE_WINDOW);

/* Help menu */
STOCK_MENU_ITEM(About,GNOME_APP_CONFIGURABLE_ITEM_ABOUT);

/* Game menu */
STOCK_MENU_ITEM(NewGame,GNOME_APP_CONFIGURABLE_ITEM_NEW_GAME);
STOCK_MENU_ITEM(PauseGame,GNOME_APP_CONFIGURABLE_ITEM_PAUSE_GAME);
STOCK_MENU_ITEM(RestartGame,GNOME_APP_CONFIGURABLE_ITEM_RESTART_GAME);
STOCK_MENU_ITEM(UndoMove,GNOME_APP_CONFIGURABLE_ITEM_UNDO_MOVE);
STOCK_MENU_ITEM(RedoMove,GNOME_APP_CONFIGURABLE_ITEM_REDO_MOVE);
STOCK_MENU_ITEM(Hint,GNOME_APP_CONFIGURABLE_ITEM_HINT);
STOCK_MENU_ITEM(Scores,GNOME_APP_CONFIGURABLE_ITEM_SCORES);
STOCK_MENU_ITEM(EndGame,GNOME_APP_CONFIGURABLE_ITEM_END_GAME);
#undef STOCK_MENU_ITEM

} //namespace MenuItems


namespace Menus
{

/* Some standard menus */


// New: If you have more than one New type then use this tree:
struct New : public Items::Menu
{
  New(const Items::Array<Info>& tree);
  ~New() {}
};


//Other menus:
#define STOCK_MENU(X,Y) \
struct X : public Items::Menu \
{ \
  X(const Items::Array<Info>& tree = Items::Array<Info>()) \
  : Items::Menu(Y,tree) \
  {} \
  ~X() \
  {} \
}

STOCK_MENU(File,"_File");
STOCK_MENU(Files,"Fi_les");
STOCK_MENU(Edit,"_Edit");
STOCK_MENU(View,"_View");
STOCK_MENU(Help,"_Help");
STOCK_MENU(Game,"_Game");
STOCK_MENU(Settings,"_Settings");
STOCK_MENU(Windows,"_Windows");
#undef STOCK_MENU

} // namespace Menus


} // namespace UI
} // namespace Gnome

#endif //LIBGNOMEUIMM_UI_ITEMS_STOCK_H
