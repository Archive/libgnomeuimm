/*
 * Copyright 2000-2002 The libgnomeuimm development team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <libgnomeuimm/ui-items-stock.h>
#include <libgnomeuimm/app-helper.h>
#include <libgnomeui/gnome-uidefs.h>
#include <gtkmm/stock.h>

namespace Gnome
{

namespace UI
{

namespace Items
{


void ConfigureItem::init(const Callback& cv, GnomeUIInfoConfigurableTypes ct)
{
  GnomeUIInfo::type = GnomeUIInfoType(GNOME_APP_UI_ITEM_CONFIGURABLE);
  InfoData* infodata = new InfoData();
  infodata->set_callback(cv);

  GnomeUIInfo::accelerator_key = ct;

  infodata->connect(*this);
}

void ConfigureItem::init(const Callback& cv, GnomeUIInfoConfigurableTypes ct, const Glib::ustring& strLabel, const Glib::ustring& strHint)
{
  GnomeUIInfo::type = GnomeUIInfoType(GNOME_APP_UI_ITEM_CONFIGURABLE);
  InfoData* infodata = new InfoData();
  infodata->set_callback(cv);

  GnomeUIInfo::accelerator_key = ct;

  infodata->label_ = strLabel; //e.g. "Create a new foo".
  infodata->hint_ = strHint; //e.g. "_New foo".

  infodata->connect(*this);
}

} // namespace Items

namespace Menus
{

New::New (const Items::Array<Items::Info>& tree)
  : Items::Menu(Items::Icon(Gtk::Stock::NEW), "_New", tree)
{
  GnomeUIInfo::accelerator_key = GNOME_KEY_NAME_NEW;
  GnomeUIInfo::ac_mods = GdkModifierType(GNOME_KEY_MOD_NEW);
}

} /* namespace Menus */

} // namespace UI
} // namespace Gnome
