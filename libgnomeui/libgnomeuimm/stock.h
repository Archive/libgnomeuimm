// -*- c++ -*-
#ifndef _LIBGNOMEUIMM_STOCK_H
#define _LIBGNOMEUIMM_STOCK_H

/* $Id$ */

/* stock.h
 *
 * Copyright (C) 2002 The gnoemmm- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/stock.h>
//namespace Gtk { class BuiltinStockID; }

namespace Gnome
{

namespace UI
{

namespace Stock
{

extern GTKMM_API const Gtk::BuiltinStockID TIMER;
extern GTKMM_API const Gtk::BuiltinStockID TIMER_STOP;
extern GTKMM_API const Gtk::BuiltinStockID TRASH;
extern GTKMM_API const Gtk::BuiltinStockID TRASH_FULL;

extern GTKMM_API const Gtk::BuiltinStockID SCORES;
extern GTKMM_API const Gtk::BuiltinStockID ABOUT;
extern GTKMM_API const Gtk::BuiltinStockID BLANK;

extern GTKMM_API const Gtk::BuiltinStockID VOLUME;
extern GTKMM_API const Gtk::BuiltinStockID MIDI;
extern GTKMM_API const Gtk::BuiltinStockID MIC;
extern GTKMM_API const Gtk::BuiltinStockID LINE_IN;

extern GTKMM_API const Gtk::BuiltinStockID MAIL;
extern GTKMM_API const Gtk::BuiltinStockID MAIL_RCV;
extern GTKMM_API const Gtk::BuiltinStockID MAIL_SND;
extern GTKMM_API const Gtk::BuiltinStockID MAIL_RPL;
extern GTKMM_API const Gtk::BuiltinStockID MAIL_FWD;
extern GTKMM_API const Gtk::BuiltinStockID MAIL_NEW;
extern GTKMM_API const Gtk::BuiltinStockID ATTACH;

extern GTKMM_API const Gtk::BuiltinStockID BOOK_RED;
extern GTKMM_API const Gtk::BuiltinStockID BOOK_GREEN;
extern GTKMM_API const Gtk::BuiltinStockID BOOK_BLUE;
extern GTKMM_API const Gtk::BuiltinStockID BOOK_YELLOW;
extern GTKMM_API const Gtk::BuiltinStockID BOOK_OPEN;

extern GTKMM_API const Gtk::BuiltinStockID MULTIPLE_FILE;
extern GTKMM_API const Gtk::BuiltinStockID NOT;

extern GTKMM_API const Gtk::BuiltinStockID TABLE_BORDERS;
extern GTKMM_API const Gtk::BuiltinStockID TABLE_FILL;

extern GTKMM_API const Gtk::BuiltinStockID TEXT_INDENT;
extern GTKMM_API const Gtk::BuiltinStockID TEXT_UNINDENT;
extern GTKMM_API const Gtk::BuiltinStockID TEXT_BULLETED_LIST;
extern GTKMM_API const Gtk::BuiltinStockID TEXT_NUMBERED_LIST;

} //namespace Stock

} //namespace UI

} //namespace Gnome


#endif //_LIBGNOMEUIMM_STOCK_H
