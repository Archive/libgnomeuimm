// $Id$ -*- c++ -*-
/*
 * Copyright 2000-2002 The libgnomeuimm development team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */


#ifndef LIBGNOMEUIMM_ICON_H
#define LIBGNOMEUIMM_ICON_H

#include <gtkmm/stockid.h>
#include <libgnomeui/gnome-app-helper.h>

namespace Gnome
{

namespace UI
{

namespace Items
{
  
/// Icons represent a standard Pixmap with various states
class Icon
{
public:
  enum Type
  {
    NONE = GNOME_APP_PIXMAP_NONE,
    STOCK = GNOME_APP_PIXMAP_STOCK,
    DATA = GNOME_APP_PIXMAP_DATA,
    FILENAME = GNOME_APP_PIXMAP_FILENAME
  };

  explicit Icon(const Gtk::StockID& stock_id);
  Icon(Type type = NONE);
  ~Icon();

  Type get_type() const;
  gconstpointer get_pixmap_info() const; //This Icon must live as long as you need to use this data.

protected:

  Type pixmap_type_;
  typedef const char * const * const xpmdata_t;

  //Only one of these will have a value:
  gconstpointer xpm_data_;
  Glib::ustring stock_id_; //There's no Gtk::StockID default constructor.
  std::string filename_;
};

class IconXpm : public Icon
{
public:
  IconXpm(xpmdata_t xpm);
  ~IconXpm();
};

class IconFile : public Icon
{
public:
  IconFile(const std::string& file);
  ~IconFile();
};

} // namespace Items
} // namespace UI
} // namespace Gnome



#endif //LIBGNOMEUIMM_ICON_H
