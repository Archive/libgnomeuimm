// -*- c++ -*-
/* $Id$ */

/* init.cc
 *
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomeuimm/init.h>
#include <libgnomeuimm/wrap_init.h>

#include <libgnomemm/init.h>
#include <libgnomeuimmconfig.h> //For LIBGNOMEMM_VERSION and LIBGNOMEUI_VERSION_NEEDED

#include <libgnomecanvasmm/wrap_init.h>
#include <gconfmm/wrap_init.h>

#include <libgnomeuimm/wrap_init.h>
#include <glibmm/init.h>
#include <gtkmm/wrap_init.h>
#include <libgnomeui/gnome-ui-init.h> //For libgnome_module_info_get()

namespace Gnome
{

namespace UI
{

static void
libgnomeuimm_post_args_parse(GnomeProgram *program, GnomeModuleInfo *mod_info)
{
  Glib::init(); //Makes sure that the registration system is set up.
  
  //Initialize dependencies:
  //These don't have ModuleInfos because we didn't want to make them dependent on libgnomemm.
  Gtk::wrap_init();
  Gnome::Canvas::wrap_init();
  Gnome::Conf::wrap_init();

  wrap_init();
}

ModuleInfo& module_info_get()
{
  static Gnome::ModuleInfo info("libgnomeuimm", LIBGNOMEUIMM_VERSION, "C++ wrappers for libgnomeui.");

  //Requirements:
  static GnomeModuleRequirement req[3];

  req[0].required_version = LIBGNOMEUI_VERSION_NEEDED_QUOTED;
  req[0].module_info = libgnomeui_module_info_get(); //The same as LIBGNOME_MODULE

  req[1].required_version = LIBGNOMEUIMM_VERSION;
  req[1].module_info = module_info_get_cpp_only().gobj(); //Initialize libgnomeuimm C++ part.

  req[2].required_version = NULL;
  req[2].module_info = NULL;

  info.set_requirements(req);

  return info;
}

ModuleInfo& module_info_get_cpp_only()
{
  static Gnome::ModuleInfo info("libgnomeuimm_cpp_only", LIBGNOMEUIMM_VERSION, "C++ wrappers for libgnomeui - C++ part only.");

  //Requirements:
  static GnomeModuleRequirement req[3];

  req[0].required_version = LIBGNOMEMM_VERSION;
  req[0].module_info = Gnome::module_info_get_cpp_only().gobj(); //The same as LIBGNOME_MODULE


  req[1].required_version = NULL;
  req[1].module_info = NULL;

  info.set_requirements(req);

  info.set_post_args_parse(&libgnomeuimm_post_args_parse);

  return info;
}

} //namespace UI
} //namespace Gnome
