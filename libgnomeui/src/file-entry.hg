/* $Id: file-entry.hg,v 1.12 2003/01/04 20:16:28 murrayc Exp $ */


/* file-entry.hg
 * 
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/box.h>
#include <gtkmm/editable.h>
//#include <libgnomeui/gnome-file-entry.h>
_DEFS(libgnomeuimm,libgnomeui)
_PINCLUDE(gtkmm/private/box_p.h)

namespace Gnome
{

namespace UI
{

class FileEntry
 : public Gtk::VBox,
   public Gtk::Editable
{
  _CLASS_GTKOBJECT(FileEntry, GnomeFileEntry, GNOME_FILE_ENTRY, Gtk::VBox, GtkVBox)
  _IMPLEMENTS_INTERFACE(Gtk::Editable)
  _IGNORE(gnome_file_entry_construct, gnome_file_entry_set_directory)
public:
  _CTOR_DEFAULT

  FileEntry(const Glib::ustring& history_id, const Glib::ustring& browse_dialog_title);

  _WRAP_METHOD(Gtk::Widget* gnome_entry(), gnome_file_entry_gnome_entry)
  _WRAP_METHOD(Gtk::Widget* gtk_entry(), gnome_file_entry_gtk_entry)

  _WRAP_METHOD(void set_title (const Glib::ustring& browse_dialog_title), gnome_file_entry_set_title)
  _WRAP_METHOD(void set_default_path(const Glib::ustring& path),gnome_file_entry_set_default_path)
  _WRAP_METHOD(void	set_directory_entry(bool directory_entry = true), gnome_file_entry_set_directory_entry)
  _WRAP_METHOD(bool get_directory_entry() const, gnome_file_entry_get_directory_entry)
  _WRAP_METHOD(Glib::ustring get_full_path(bool file_must_exist) const, gnome_file_entry_get_full_path)
  _WRAP_METHOD(void set_filename(const Glib::ustring& filename), gnome_file_entry_set_filename)
  _WRAP_METHOD(void set_modal(bool is_modal = true), gnome_file_entry_set_modal)
  _WRAP_METHOD(bool get_modal() const, gnome_file_entry_get_modal)

  _WRAP_SIGNAL(void browse_clicked(), "browse_clicked");
  _WRAP_SIGNAL(void activate(), "activate");
};

} /* namespace UI */
} /* namespace Gnome */ 



