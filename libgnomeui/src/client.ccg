/* $Id: client.ccg,v 1.7 2002/01/19 04:26:33 daniel Exp $ */


/* client.ccg
 * 
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/dialog.h>

namespace Gnome
{

namespace UI
{

Client::Client(bool connect_to_sm)
  :  Gtk::Object(GTK_OBJECT(g_object_new(get_type(),0)))
{
  gobj()->program = 0; // this should normally be program_invocation_name

  if(connect_to_sm)
    connect_to_session_manager();
}

void
Client::set_restart_command (const Glib::ArrayHandle<Glib::ustring>& argv)
{
  gnome_client_set_restart_command(gobj(), argv.size(), const_cast<char**>(argv.data()));
}

void 
Client::set_discard_command(const Glib::ArrayHandle<Glib::ustring>& argv)
{
  gnome_client_set_discard_command(gobj(), argv.size(), const_cast<char**>(argv.data()));
}

void 
Client::set_shutdown_command(const Glib::ArrayHandle<Glib::ustring>& argv)
{
  gnome_client_set_shutdown_command(gobj(), argv.size(), const_cast<char**>(argv.data()));
}

void  
Client::set_resign_command (const Glib::ArrayHandle<Glib::ustring>& argv)
{
  gnome_client_set_resign_command(gobj(), argv.size(), const_cast<char**>(argv.data()));
}

void  
Client::set_clone_command (const Glib::ArrayHandle<Glib::ustring>& argv)
{
  gnome_client_set_clone_command(gobj(), argv.size(),
				 const_cast<char**>(argv.data()));
}

bool
Client::is_connected()
{
  return GNOME_CLIENT_CONNECTED(gobj());
}

} /* namespace UI */
} /* namespace Gnome */

