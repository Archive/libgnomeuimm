// -*- C++ -*- // this is for the .hg, I realize gensig puts one in
/* $Id: icon-list.hg,v 1.18 2004/06/06 21:13:08 murrayc Exp $ */

/* icon-list.hg
 * 
 * Copyright (C) 1998 EMC Capital Management Inc.
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomeuimm/icon-textitem.h>
#include <libgnomecanvasmm/canvas.h>
#include <libgnomecanvasmm/pixbuf.h>
#include <gtkmm/container.h>
#include <glibmm/helperlist.h>
#include <libgnomeui/gnome-icon-list.h>
_DEFS(libgnomeuimm,libgnomeui)
_PINCLUDE(libgnomecanvasmm/private/canvas_p.h)

namespace Gnome
{

namespace UI
{

namespace IconList_Helpers
{

#m4 include(list.m4)
  GP_LIST(SelectionList,IconList,GnomeIconList,int,CUSTOM)
  GP_LIST_ELEM(int)
protected:
  mutable GList* glist_;
  GP_LIST_END()

} /* namespace IconList_Helpers */


class Group;

class IconList : public Gnome::Canvas::Canvas
{
  _CLASS_GTKOBJECT(IconList, GnomeIconList, GNOME_ICON_LIST, Gnome::Canvas::Canvas, GnomeCanvas)
  _IGNORE(gnome_icon_list_construct)
public:
  //TODO: add missing ctor args
  explicit IconList(guint icon_width = 80, bool is_editable = false);

  _WRAP_METHOD(void set_hadjustment(Gtk::Adjustment& hadj), gnome_icon_list_set_hadjustment)
  _WRAP_METHOD(void set_vadjustment(Gtk::Adjustment& vadj), gnome_icon_list_set_vadjustment)
  _WRAP_METHOD(void freeze(), gnome_icon_list_freeze)
  _WRAP_METHOD(void thaw(), gnome_icon_list_thaw)
  _WRAP_METHOD(void insert(int pos, const Glib::ustring& icon_filename, const Glib::ustring& text), gnome_icon_list_insert)
  _WRAP_METHOD(void insert(int idx, const Glib::RefPtr<Gdk::Pixbuf>& im, const Glib::ustring& icon_filename, const Glib::ustring& text), gnome_icon_list_insert_pixbuf)
  _WRAP_METHOD(int append(const Glib::ustring& icon_filename, const Glib::ustring& text), gnome_icon_list_append)
  _WRAP_METHOD(int append(const Glib::RefPtr<Gdk::Pixbuf>& im, const Glib::ustring& icon_filename, const Glib::ustring& text), gnome_icon_list_append_pixbuf)
  _WRAP_METHOD(void clear(), gnome_icon_list_clear)
  _WRAP_METHOD(void remove(int pos), gnome_icon_list_remove)
  _WRAP_METHOD(guint get_num_icons() const, gnome_icon_list_get_num_icons)
  _WRAP_METHOD(Gtk::SelectionMode get_selection_mode() const, gnome_icon_list_get_selection_mode)
  _WRAP_METHOD(void set_selection_mode(Gtk::SelectionMode mode), gnome_icon_list_set_selection_mode)
  _WRAP_METHOD(void select_icon(int idx), gnome_icon_list_select_icon)
  _WRAP_METHOD(void unselect_icon(int idx), gnome_icon_list_unselect_icon)
  _WRAP_METHOD(int select_all(), gnome_icon_list_unselect_all)
  _WRAP_METHOD(int unselect_all(), gnome_icon_list_unselect_all)
  _WRAP_METHOD(void focus_icon(gint idx), gnome_icon_list_focus_icon)
  _WRAP_METHOD(void set_icon_width (int w), gnome_icon_list_set_icon_width)
  _WRAP_METHOD(void set_row_spacing(int spacing), gnome_icon_list_set_row_spacing)
  _WRAP_METHOD(void set_col_spacing(int spacing), gnome_icon_list_set_col_spacing)
  _WRAP_METHOD(void set_text_spacing(int spacing), gnome_icon_list_set_text_spacing)
  _WRAP_METHOD(void set_icon_border(int spacing), gnome_icon_list_set_icon_border)
  _WRAP_METHOD(void set_separators(const Glib::ustring& sep), gnome_icon_list_set_separators)
  _WRAP_METHOD(Glib::ustring get_icon_filename(int idx) const, gnome_icon_list_get_icon_filename)
  _WRAP_METHOD(int find_icon_from_filename(const Glib::ustring& filename) const, gnome_icon_list_find_icon_from_filename)
  _WRAP_METHOD(void set_icon_data(int pos, gpointer data), gnome_icon_list_set_icon_data)
  _IGNORE(gnome_icon_list_set_icon_data_full)
  _WRAP_METHOD(int find_icon_from_data(gpointer data) const, gnome_icon_list_find_icon_from_data)
  _WRAP_METHOD(gpointer get_icon_data(int pos), gnome_icon_list_get_icon_data)
  _WRAP_METHOD(void moveto(int pos, double yalign), gnome_icon_list_moveto)
  _WRAP_METHOD(Gtk::Visibility icon_is_visible(int pos), gnome_icon_list_icon_is_visible)
  _WRAP_METHOD(int get_icon_at(int x, int y) const, gnome_icon_list_get_icon_at)
  _WRAP_METHOD(int get_items_per_line() const, gnome_icon_list_get_items_per_line)

  _WRAP_METHOD(IconTextItem* get_icon_text_item(int idx), gnome_icon_list_get_icon_text_item)
  _WRAP_METHOD(Gnome::Canvas::Pixbuf* get_icon_pixbuf_item(int idx), gnome_icon_list_get_icon_pixbuf_item)

  typedef IconList_Helpers::SelectionList SelectionList;
  SelectionList& selection();
  const SelectionList& selection() const;
  _IGNORE(gnome_icon_list_get_selection)

  _WRAP_SIGNAL(void select_icon(int num, GdkEvent* event), "select_icon");
  _WRAP_SIGNAL(void unselect_icon(int num, GdkEvent* event), "unselect_icon");

  _WRAP_SIGNAL(void focus_icon(int num), "focus_icon");

  _WRAP_SIGNAL(bool text_changed(int num, const char* new_text), "text_changed");

	/* Key Binding signals */
	_WRAP_SIGNAL(void move_cursor(Gtk::DirectionType dir, bool clear_selection), "move_cursor")
	_WRAP_SIGNAL(void toggle_cursor_selection(), "toggle_cursor_selection")

protected:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  mutable SelectionList selection_proxy_;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
};

} /* namespace UI */
} /* namespace Gnome */
