/* $Id: druid-page-standard.hg,v 1.13 2004/06/06 21:13:08 murrayc Exp $ */


/* druid-page-standard.hg
 * 
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomeuimm/druid-page.h>
#include <gtkmm/box.h>
#include <libgnomeui/gnome-druid-page-standard.h>
_DEFS(libgnomeuimm,libgnomeui)
_PINCLUDE(libgnomeuimm/private/druid-page_p.h)

namespace Gnome
{

namespace UI
{

class DruidPageStandard : public DruidPage
{
  _CLASS_GTKOBJECT(DruidPageStandard, GnomeDruidPageStandard, GNOME_DRUID_PAGE_STANDARD, DruidPage, GnomeDruidPage)
  _IGNORE(gnome_druid_page_standard_construct, gnome_druid_page_standard_set_bg_color, gnome_druid_page_standard_set_logo_bg_color, gnome_druid_page_standard_set_title_color)
public:

  DruidPageStandard();
  DruidPageStandard(const Glib::ustring& title, const Glib::RefPtr<Gdk::Pixbuf>& logo_image, const Glib::RefPtr<Gdk::Pixbuf>& top_watermark);

  _WRAP_METHOD(void set_title(const Glib::ustring& title), gnome_druid_page_standard_set_title)
  _WRAP_METHOD(void set_logo(const Glib::RefPtr<Gdk::Pixbuf>& logo_image), gnome_druid_page_standard_set_logo)
  _WRAP_METHOD(void set_top_watermark(const Glib::RefPtr<Gdk::Pixbuf>& top_watermark_image), gnome_druid_page_standard_set_top_watermark)
  _WRAP_METHOD(void set_title_foreground(const Gdk::Color& color), gnome_druid_page_standard_set_title_foreground)
  _WRAP_METHOD(void set_background (const Gdk::Color& color), gnome_druid_page_standard_set_background)
  _WRAP_METHOD(void set_logo_background(const Gdk::Color& color), gnome_druid_page_standard_set_logo_background)
  _WRAP_METHOD(void set_contents_background(const Gdk::Color& color), gnome_druid_page_standard_set_contents_background)
  _WRAP_METHOD(void append_item(const Glib::ustring& question_mnemonic, Gtk::Widget& item, const Glib::ustring& additional_info_markup), gnome_druid_page_standard_append_item)

  /// Pack widgets into the VBox.
	_MEMBER_GET(vbox, vbox, Gtk::VBox*, GtkWidget*);
 	_MEMBER_GET(title, title, Glib::ustring, gchar*)
 	_MEMBER_GET(logo, logo, Glib::RefPtr<Gdk::Pixbuf>, GdkPixbuf*)
 	_MEMBER_GET(top_watermark, top_watermark, Glib::RefPtr<Gdk::Pixbuf>, GdkPixbuf*)
 	_MEMBER_GET(title_foreground, title_foreground, Gdk::Color, GdkColor)
 	_MEMBER_GET(background, background, Gdk::Color, GdkColor)
 	_MEMBER_GET(logo_background, logo_background, Gdk::Color, GdkColor)
 	_MEMBER_GET(contents_background, contents_background, Gdk::Color, GdkColor)

  _WRAP_PROPERTY("title", Glib::ustring)
  _WRAP_PROPERTY("logo", Gdk::Pixbuf)
  _WRAP_PROPERTY("top-watermark", Gdk::Pixbuf)
  _WRAP_PROPERTY("title-foreground", Glib::ustring)
  _WRAP_PROPERTY("title-foreground-gdk", Gdk::Color)
  _WRAP_PROPERTY("title-foreground-set", bool)
  _WRAP_PROPERTY("background", Glib::ustring)
  _WRAP_PROPERTY("background-gdk", Gdk::Color)
  _WRAP_PROPERTY("background-set", bool)
  _WRAP_PROPERTY("logo-background", Glib::ustring)
  _WRAP_PROPERTY("logo-background-gdk", Gdk::Color)
  _WRAP_PROPERTY("contents_background", Glib::ustring)
  _WRAP_PROPERTY("contents_background_gdk", Gdk::Color)
  _WRAP_PROPERTY("contents_background_set", bool)
    
       
};

} /* namespace UI */
} /* namespace Gnome */